#Segunda parte del análisis del medicamento Risperidona

#Directorio de trabajo
setwd("D:/DANIELR/PROYECTOS/EPS_RESPIRIDONA_ANALYSIS/DESARROLLO_ANALITICO/2-DATA_BASE")

#Carga de la data
library(readr)
library(dplyr)
Data_Risp <- read_delim("Risperidona.txt",
                          "|", escape_double = FALSE, trim_ws = TRUE)


# filtro de la data objetivo
# estado de autorización == PAGADA

dim(Data_Risp)
names(Data_Risp)

# filtramos la data en = PAGADA

Data_PAG<-Data_Risp %>% filter(Estado_Autorizacion_Desc=="PAGADA")

dim(Data_PAG)

#Limpieza de la data

#Limpiando la base
#Quitamos N'A's de la variable unica Numero de documento

Data_PAG<-Data_PAG %>% filter(NUMERO_DE_DOCUMENTO!=is.na(Data_PAG$NUMERO_DE_DOCUMENTO))
Data_PAG<-Data_PAG %>% filter(Valor_Pagado!=is.na(Data_PAG$Valor_Pagado))
Data_PAG<-Data_PAG %>% filter(CANTIDAD_AUTORIZADA!=is.na(Data_PAG$CANTIDAD_AUTORIZADA))
Data_PAG<-Data_PAG %>% filter(Valor_Reservado!=is.na(Data_PAG$Valor_Reservado))


#Data Valor pagado diferente de 1
summary(Data_PAG$Valor_Pagado)
summary(Data_PAG$CANTIDAD_AUTORIZADA)




#SELECCIONANDO LAS VARIABLES

Data1<-Data_PAG[,c(23,3,9,12,13,22,25,26,28,30,31,37,39,40,42,44,47,49,51,57,61,66,67,71)]

dim(Data1)
names(Data1)
summary(Data1$EDAD)
summary(Data1$CANTIDAD_AUTORIZADA)
summary(Data1$Valor_Pagado)
summary(Data1$Valor_Provision)
#grupo etario vs cantidad autorizda
Data1$Grupo_Etario_Desc<-as.factor(Data1$Grupo_Etario_Desc)
par(mar=c(2.5,2.5,2.5,2.5))
plot(Data1$CANTIDAD_AUTORIZADA~Data1$Grupo_Etario_Desc,
     xlab="Descripcion por grupo Etario",
     ylab = "Cantidad", main="Cantidad autorizada vs Grupo Etario")

#origen de prestadora de servicio
Data1$Agrup_Salud_Prest_Desc<-as.factor(Data1$Agrup_Salud_Prest_Desc)
plot(Data1$CANTIDAD_AUTORIZADA~Data1$Agrup_Salud_Prest_Desc,
     xlab="Descripcion por Salud prestadora de servicio",
     ylab = "Cantidad", main="Cantidad autorizada vs prestadora de servicio")

#origen de servicio
Data1$Origen_Servicio_Desc<-as.factor(Data1$Origen_Servicio_Desc)
plot(Data1$CANTIDAD_AUTORIZADA~Data1$Origen_Servicio_Desc,
     xlab="Descripcion por Salud origen de servicio",
     ylab = "Cantidad", main="Cantidad autorizada vs origen de servicio")



#distribucion por origen de servicio por total clientes
Data1$NUMERO_DE_DOCUMENTO<-as.character(Data1$NUMERO_DE_DOCUMENTO)
Origen_enfermedad<-Data1 %>% select(Origen_Servicio_Desc,NUMERO_DE_DOCUMENTO) %>% distinct(NUMERO_DE_DOCUMENTO, .keep_all = TRUE) %>%  group_by(Origen_Servicio_Desc) %>% summarise_all(funs(n()))


#distribucion por origen de servicio
Data1$NUMERO_DE_DOCUMENTO<-as.character(Data1$NUMERO_DE_DOCUMENTO)
Salud_Prest_Desc<-Data1 %>% select(Agrup_Salud_Prest_Desc,NUMERO_DE_DOCUMENTO) %>% distinct(NUMERO_DE_DOCUMENTO, .keep_all = TRUE) %>%  group_by(Agrup_Salud_Prest_Desc) %>% summarise_all(funs(n()))


Data2<-Data1[,c(2,4,5,8,9,11,12,15,19,20)]
Data2$NUMERO_DE_DOCUMENTO<-as.character(Data2$NUMERO_DE_DOCUMENTO)
#total clientes
Total_clientes<-Data2[1]
Total_clientes<- Total_clientes %>% distinct(NUMERO_DE_DOCUMENTO, .keep_all = TRUE)

#saber cuantos clientes hay por frecuencia de cantidad registrada
Data2 <- Data1 %>% select(CANTIDAD_AUTORIZADA,NUMERO_DE_DOCUMENTO) %>%  group_by(CANTIDAD_AUTORIZADA) %>% summarise_all(funs(n()))
colnames(Data2)[2]<-"Total_pacientes"
Data2$CANTIDAD_AUTORIZADA<-as.factor(Data2$CANTIDAD_AUTORIZADA)

library(ggplot2)
#Grafica para ver la distribucion de pacientes por cantidad
p<-ggplot(data = Data2, aes(x=CANTIDAD_AUTORIZADA, y = Total_pacientes )) + geom_bar(stat="identity")

p + coord_flip()


Data2 <- Data1 %>% select(CANTIDAD_AUTORIZADA,NUMERO_DE_DOCUMENTO) %>%  group_by(CANTIDAD_AUTORIZADA) %>% summarise_all(funs(n()))

#Grafica para ver la distribucion por grupo de acuerdo al numero de orden

Cant_orden <- Data1 %>% select(Grupo_Etario_Desc,Numero_Consec_Orden_Serie) %>%  group_by(Grupo_Etario_Desc) %>% summarise_all(funs(n()))


c<-ggplot(data = Cant_orden, aes(x=Grupo_Etario_Desc, y = Numero_Consec_Orden_Serie )) + geom_bar(stat="identity")

c + coord_flip()



#descripcion prestacion


Descrip_OP <- Data1 %>% select(CODIGO_PRESTACION_OP,NUMERO_DE_DOCUMENTO) %>%  group_by(CODIGO_PRESTACION_OP) %>% summarise_all(funs(n()))

Descrip_OP$Prestacion_op<-Data1$DESCRIPCION_PRESTACION[match(Descrip_OP$CODIGO_PRESTACION_OP,Data1$CODIGO_PRESTACION_OP)]


#regional

regional <- Data1 %>% select(DESC_REGIONAL_IPS_AFILIADO,Numero_Consec_Orden_Serie) %>%  group_by(DESC_REGIONAL_IPS_AFILIADO) %>% summarise_all(funs(n()))

library(ggplot2)
par(mar=c(4,3,3,4))
regional %>%
  arrange(desc(Numero_Consec_Orden_Serie)) %>%
  mutate(Region = factor(DESC_REGIONAL_IPS_AFILIADO, DESC_REGIONAL_IPS_AFILIADO)) %>%
  ggplot(aes(x=DESC_REGIONAL_IPS_AFILIADO, y=Numero_Consec_Orden_Serie, size=Numero_Consec_Orden_Serie, color=DESC_REGIONAL_IPS_AFILIADO)) +
  geom_point(alpha=0.5) +
  scale_size(range = c(.1, 24), name="Numero de ordenes generadas")

#por diagnostico
diagnostic <- Data1 %>% select(Diagnostico_EPS_Desc,Numero_Consec_Orden_Serie) %>%  group_by(Diagnostico_EPS_Desc) %>% summarise_all(funs(n()))

diagnostic<-diagnostic %>% arrange(desc(Numero_Consec_Orden_Serie))




#190 diagnositicos diferentes
#solo escogeriamos los primeros 3
diagnostic_five<-diagnostic[c(1:3),1:2]
diagnostic_five<-as.data.frame(diagnostic_five)
diagnostic_five$Diagnostico_EPS_Desc<-as.factor(diagnostic_five$Diagnostico_EPS_Desc)
diagnostic_five$Numero_Consec_Orden_Serie<-as.numeric(diagnostic_five$Numero_Consec_Orden_Serie)

par(mar=c(6,4,4,4))


plot(diagnostic_five$Diagnostico_EPS_Desc~ diagnostic_five$Numero_Consec_Orden_Serie)

d<-ggplot(data = diagnostic_five, aes(x=Diagnostico_EPS_Desc, y = Numero_Consec_Orden_Serie )) + geom_bar(stat="identity")

d + coord_flip()

#descripcion por sucursal que emite


sucursal <- Data1 %>% select(DESCRIPCION_SUCURSAL_EMITE,Numero_Consec_Orden_Serie) %>%
  group_by(DESCRIPCION_SUCURSAL_EMITE)  %>%
  summarise_all(funs(n())) %>% arrange(desc(Numero_Consec_Orden_Serie))

top10_sucursal<- sucursal %>%
  filter(rank(desc(Numero_Consec_Orden_Serie))<=10)

library(ggplot2)
par(mar=c(6,3,3,6))
top10_sucursal %>%
  mutate(Region = factor(DESCRIPCION_SUCURSAL_EMITE, DESCRIPCION_SUCURSAL_EMITE)) %>%
  ggplot(aes(x=DESCRIPCION_SUCURSAL_EMITE, y=Numero_Consec_Orden_Serie, size=Numero_Consec_Orden_Serie, color=DESCRIPCION_SUCURSAL_EMITE)) +
  geom_point(alpha=0.5) +
  scale_size(range = c(.1, 24), name="Numero de ordenes generadas")

#Prestador quien atiende

prestador <- Data1 %>% select(DESCRIPCION_PRESTADOR_ATIENDE,Numero_Consec_Orden_Serie) %>%
  group_by(DESCRIPCION_PRESTADOR_ATIENDE)  %>%
  summarise_all(funs(n())) %>% arrange(desc(Numero_Consec_Orden_Serie))

library(tidyverse)

par(mar=c(1,1,1,1))
top05_prestador<- prestador %>%
  filter(rank(desc(Numero_Consec_Orden_Serie))<=5)

ggplot(top05_prestador, aes(x=DESCRIPCION_PRESTADOR_ATIENDE, y=Numero_Consec_Orden_Serie)) +
  geom_segment( aes(x=DESCRIPCION_PRESTADOR_ATIENDE, xend=DESCRIPCION_PRESTADOR_ATIENDE, y=0, yend=Numero_Consec_Orden_Serie)) +
  geom_point( size=5, color="red", fill=alpha("orange", 0.3), alpha=0.7, shape=21, stroke=2)


#---- relacion variables-----------------
library(dplyr)

Data1$PERIODO<-as.character(Data1$PERIODO)
tiempo_ordenes <- Data1 %>% select(PERIODO,Numero_Consec_Orden_Serie) %>%
  group_by(PERIODO)  %>%
  summarise_all(funs(n()))

library(car)
library(lattice)
library(FactoMineR)
library(lubridate)
library(timeSeries)
library(xts)

tiempo_ordenes$Year<-substr(tiempo_ordenes$PERIODO,1,4)
tiempo_ordenes$Month<-substr(tiempo_ordenes$PERIODO,5,6)
tiempo_ordenes$Decimal_date<-paste0(tiempo_ordenes$Year,".",tiempo_ordenes$Month)
#organizamos las columnas
tiempo_ordenes<-tiempo_ordenes[,c(3:5,1:2)]
tiempo_ordenes$Decimal_date<-as.numeric(tiempo_ordenes$Decimal_date)

#convertir el formato decimal_date en formato fecha


dd<-date_decimal(tiempo_ordenes$Decimal_date)
dd
par(mar=c(6,4,4,6))
hist(tiempo_ordenes$Numero_Consec_Orden_Serie, main = "Distribución de las ordenes generadas", xlab = "Ordenes")
plot(density(tiempo_ordenes$Numero_Consec_Orden_Serie), main = "Estimación de la densidad de las ordenes generadas")

summary(tiempo_ordenes$Numero_Consec_Orden_Serie)

par(mar=c(6,4,4,6))
Valor_view<-ts(tiempo_ordenes$Numero_Consec_Orden_Serie, start = c(2018,01,04), end = c(2019,01,22), frequency = 12)
plot(Valor_view, col='red')

#cantidad de ordenes por paciente
Cant_ordenes_X_paciente<-Data1 %>% select(NUMERO_DE_DOCUMENTO,Numero_Consec_Orden_Serie) %>%
  group_by(NUMERO_DE_DOCUMENTO)  %>%
  summarise_all(funs(n())) %>% arrange(desc(Numero_Consec_Orden_Serie))

library(tidyverse)
library(dplyr)

par(mar=c(1,1,1,1))
options("scipen"=100, "digits"=4)
top05_ord_x_paciente<- Cant_ordenes_X_paciente %>%
                       filter(rank(desc(Numero_Consec_Orden_Serie))<=5)

top05_ord_x_paciente$NUMERO_DE_DOCUMENTO<-as.character(top05_ord_x_paciente$NUMERO_DE_DOCUMENTO)
top05_ord_x_paciente$Numero_Consec_Orden_Serie<-as.numeric(top05_ord_x_paciente$Numero_Consec_Orden_Serie)
O<-ggplot(data = top05_ord_x_paciente, aes(x=NUMERO_DE_DOCUMENTO, y = Numero_Consec_Orden_Serie )) + geom_bar(stat="identity")

O + coord_flip()

summary(Cant_ordenes_X_paciente$Numero_Consec_Orden_Serie)

write.csv(Cant_ordenes_X_paciente, file = "id_pacientes_x_ordenes.csv")

#cantidad de ordenes
Cant_ordenes_X_paciente$Numero_Consec_Orden_Serie<-as.character(Cant_ordenes_X_paciente$Numero_Consec_Orden_Serie)

agrup_ordenes<-Cant_ordenes_X_paciente %>% select(NUMERO_DE_DOCUMENTO,Numero_Consec_Orden_Serie) %>%
  group_by(Numero_Consec_Orden_Serie)  %>%
  summarise_all(funs(n())) %>% arrange((NUMERO_DE_DOCUMENTO))
colnames(agrup_ordenes)[2]<-"Total_pacientes"

write.csv(agrup_ordenes, file = "Agrup_ordenes_x_paciente.csv")
#CAntidad por ordenes

Cant_x_orden<-Data1[,c(1,23)]
write.csv(Cant_x_orden, file = "Id_orden_x_cantidad.csv")
agrup_cant<-Cant_x_orden %>% select(CANTIDAD_AUTORIZADA,Numero_Consec_Orden_Serie) %>%
  group_by(CANTIDAD_AUTORIZADA)  %>%
  summarise_all(funs(n())) %>% arrange(desc(Numero_Consec_Orden_Serie))
colnames(agrup_cant)[2]<-"Total_orden"

write.csv(sucursal, file = "sucursales_ordenes_.csv")

write.csv(diagnostic, file = "diagnostic_x_orden_.csv")
